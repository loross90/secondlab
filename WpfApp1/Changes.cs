﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    public class Changes
    {
        public string Id { get; set; }
        public string Was { get; set; }
        public string Became { get; set; }
    }
}
