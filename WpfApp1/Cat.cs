﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    public class Cat
    {
        static int col = 0;
        public int Id { get; set; }
        public string Name { get; set; }

        public Cat(string name) 
        {
            col++;
            Id = col; 
            Name = name;
        }
    }
}
